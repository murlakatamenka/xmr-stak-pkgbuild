#!/bin/bash

# generate .SRCINFO
makepkg --printsrcinfo > .SRCINFO
# alternatively
# makepkg --printsrcinfo | tee .SRCINFO

# build and install package
makepkg -si

# to install built package manually
# sudo pacman -U xmr-stak*.pkg.tar.*
