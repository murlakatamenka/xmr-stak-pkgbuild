# xmr-stak-makepkg

`PKGBUILD` for those who want to build [`xmr-stak`](https://github.com/fireice-uk/xmr-stak) manually and install it on their system.

Also a demonstration that it's really not hard to create AUR packages and thus contribute back to the awesome Arch Linux community.

## Usage

Installation, updating

```bash
./xmr-stak-install.sh
```

It will clone / fetch `xmr-stak` git repo, build it into a package and install via pacman.

## Modification

### `xmr-stak`'s Build Options

Refer to [the official documentation](https://github.com/fireice-uk/xmr-stak/blob/master/doc/compile.md#generic-build-options) for availble options. You can tweak them in `PKGBUILD`'s `build()` function. Don't forget to update dependencies (`makedepends`, `depends`), if building `xmr-stak` with CUDA support, for example.

### Adjust Donation Level

1. Edit `xmr-stak/xmrstak/donate-level.hpp`

    ```bash
    nano xmr-stak/xmrstak/donate-level.hpp
    ```

    to change `fDevDonationLevel` to your desired value

    ```cpp
    constexpr double fDevDonationLevel = 2.0 / 100.0;
    ```

2. Update `donate-level.patch`

    ```bash
    git -C "xmr-stak" diff > donate-level.patch
    ```

3. Finally update sha256 checksum in `PKGBUILD`

    ```bash
    updpkgsums
    ```

## Useful resources

- `xmr-stak` AUR packages:  
https://aur.archlinux.org/packages/?K=xmr%2Dstak 
(they helped to build this `PKBUILD`)
- Arch Wiki <3
    - [Creating packages](https://wiki.archlinux.org/index.php/Creating_packages)
    - [PKGBUILD](https://wiki.archlinux.org/index.php/PKGBUILD)
    - [makepkg](https://wiki.archlinux.org/index.php/Makepkg)

- [`namcap`](https://wiki.archlinux.org/index.php/Namcap) (utility to check `PKGBUILD`'s)
